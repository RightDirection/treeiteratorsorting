<?php

require_once (__DIR__.'/../vendor/autoload.php');

use Fitcher\Entity\{
    Branch,
    Leaf
};

$ROOT = new Branch();

$ROOT->setLeft(new Branch());
$ROOT->getLeft()->setLeft(new Branch());
$ROOT->getLeft()->setRight(new Branch());

$leaf = new Leaf(7);
$leaf->insertLast(1);
$leaf->insertLast(1);
$leaf->insertLast(2);

$ROOT->getLeft()->getRight()->setLeafs($leaf);

$leaf = new Leaf(1);
$leaf->insertLast(1);
$leaf->insertLast(10);
$leaf->insertLast(70);

$ROOT->getLeft()->getLeft()->setLeafs($leaf);
print("Base tree\n");
print_r($ROOT);
// makeSorting
$ROOT->recursiveGoint(4);
print("result tree\n");
print_r($ROOT);
print("Result tail\n");
print_r($ROOT::$TAIL);
