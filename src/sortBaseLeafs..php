<?php

require_once (__DIR__.'/../vendor/autoload.php');

use Fitcher\Entity\{
    Branch,
    Leaf
};

$ROOT = new Branch();

/*$ROOT->setLeft(new Branch());
$ROOT->getLeft()->setLeft(new Branch());
$ROOT->getLeft()->setRight(new Branch());
$leaf = new Leaf(rand(1,100));
$leaf->setNext(new Leaf(rand(1,100)));
$leaf->getNext()->setNext(new Leaf(rand(1,100)));
$leaf->getNext()->getNext()->setNext(new Leaf(rand(1,100)));
$ROOT->getLeft()->getRight()->setLeafs($leaf);
print_r($ROOT);*/
$leaf = new Leaf(mt_rand(1,100));
$leaf->setNext(new Leaf(mt_rand(1,100)));
$leaf->getNext()->setNext(new Leaf(mt_rand(1,100)));
$leaf->getNext()->getNext()->setNext(new Leaf(mt_rand(1,100)));
$ROOT->setLeafs($leaf);

//print_r($ROOT->getLeafs());
print_r($ROOT->sortLeafs());
//var_dump($ROOT->getLeafs());
