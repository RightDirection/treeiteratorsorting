<?php

namespace Fitcher\Entity;

use Fitcher\Entity\Contract\LeafContract;

/**
 * Class Leaf
 * @package Fitcher\Entity
 */
class Leaf implements LeafContract
{
    /**
     * @var int
     */
    protected $value = 0;
    /**
     * @var Leaf|null
     */
    protected $next = null;

    /**
     * Leaf constructor.
     * @param int $value
     * @param LeafContract|null $next
     */
    public function __construct(int $value = 0, LeafContract $next = null)
    {
        $this->value = $value;
        $this->next = $next;
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }

    /**
     * @param int $value
     */
    public function setValue(int $value): void
    {
        $this->value = $value;
    }

    /**
     * @return Leaf
     */
    public function getNext()
    {
        return $this->next;
    }

    /**
     * @param LeafContract $next
     */
    public function setNext(LeafContract $next = null)
    {
        $this->next = $next;
    }

    /**
     * @param int $number
     */
    public function insertNext(int $number)
    {
        $this->setNext(new Leaf($number));
    }

    /**
     * @return bool
     */
    public function hasNext()
    {
        return $this->next instanceof Leaf;
    }

    /**
     * @return $this|Leaf
     */
    public function &returnLinkToLastLeaf()
    {
        if ($this->getNext() === null)
            return $this;

        return $this->getNext()->returnLinkToLastLeaf();
    }

    /**
     * @param $number
     */
    public function insertLast(int $number)
    {
        $this->returnLinkToLastLeaf()->insertNext($number);
    }

    public function insertLastLikeObject(Leaf $data)
    {
        $this->returnLinkToLastLeaf()->setNext($data);
    }
}