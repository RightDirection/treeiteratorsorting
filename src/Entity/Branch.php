<?php

namespace Fitcher\Entity;

use Fitcher\Entity\Contract\BranchContract;
use Fitcher\Entity\Contract\LeafContract;

/**
 * Class Branch
 * @package Fitcher\Entity
 */
class Branch implements BranchContract
{
    /**
     * @var Branch|null
     */
    protected $left = null;
    /**
     * @var Branch|null
     */
    protected $right = null;
    /**
     * @var Leaf|null
     */
    protected $leafs = null;

    /**
     * @var null
     */
    static $TAIL = null;

    /**
     * Branch constructor.
     * @param BranchContract|null $left
     * @param BranchContract|null $right
     * @param LeafContract|null $leafs
     */
    public function __construct(
        BranchContract $left = null,
        BranchContract $right = null,
        LeafContract $leafs = null
    )
    {
        $this->left = $left;
        $this->right = $right;
        $this->leafs = $leafs;
    }

    /**
     * @return Branch
     */
    public function getLeft()
    {
        return $this->left;
    }

    /**
     * @param BranchContract $left
     */
    public function setLeft(BranchContract $left)
    {
        $this->left = $left;
    }

    /**
     * @return Branch
     */
    public function getRight()
    {
        return $this->right;
    }

    /**
     * @param BranchContract $right
     */
    public function setRight(BranchContract $right)
    {
        $this->right = $right;
    }

    /**
     * @return bool
     */
    public function hasLeafs(): bool
    {
        return $this->leafs instanceof Leaf;
    }

    /**
     * @return Leaf
     */
    public function sortLeafs(): Leaf
    {
        $root = $this->leafs;
        $new_root = null;

        while ($root !== null) {
            $node = $root;
            $root = $root->getNext();

            if ($new_root == null || $node->getValue() < $new_root->getValue()) {
                $node->setNext($new_root);
                $new_root = $node;
            } else {
                $current = $new_root;
                $current = $this->findNextBigger($current, $node);

                $node->setNext($current->getNext());
                $current->setNext($node);
            }
        }
        return $new_root;
    }

    /**
     * @param $current
     * @param $node
     * @return mixed
     */
    protected function findNextBigger($current, $node)
    {
        while ($current->getNext() != null && !($node->getValue() < $current->getNext()->getValue())) {
            $current = $current->getNext();
        }
        return $current;
    }

    /**
     * @param $MAX
     * @return Leaf|null
     */
    public function recursiveGoint($MAX)
    {
        if ($this->getLeft() !== null)
            $this->getLeft()->recursiveGoint($MAX);
        if ($this->getRight() !== null)
            $this->getRight()->recursiveGoint($MAX);
        if ($this->getLeafs() !== null) {
            $this->insertTailToEnd(self::$TAIL);
            $this->leafs = $this->sortLeafs();
            self::$TAIL = $this->getMaxAndReturnTail($MAX);
        }
    }

    /**
     * @param LeafContract|null $tail
     * @return void
     */
    protected function insertTailToEnd($tail = null)
    {
        if ($tail !== null)
            $this->leafs->insertLastLikeObject($tail);
    }

    /**
     * @param $MAX
     * @return Leaf|null
     */
    protected function getMaxAndReturnTail($MAX)
    {
        $sum = 0;
        $tail = null;
        $root = $this->getLeafs();
        $node = null;
        $new_root = null;
        while ($root !== null && $sum <= $MAX && $MAX >= $sum + $root->getValue()) {
            $node = $root;
            $root = $node->getNext();

            if ($new_root === null) {
                $new_root = new Leaf($node->getValue());
            } else {
                $new_root->insertLast($node->getValue());
            }

            $sum += $node->getValue();
        }
        $this->setLeafs($new_root);

        return $root;
    }

    /**
     * @return Leaf
     */
    public function getLeafs()
    {
        return $this->leafs;
    }

    /**
     * @param LeafContract|Leaf $leafs
     */
    public function setLeafs(LeafContract $leafs)
    {
        $this->leafs = $leafs;
    }
}