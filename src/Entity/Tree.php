<?php

namespace Fitcher\Entity;


use Fitcher\Entity\Contract\{
    BranchContract,
    TreeContract
};

/**
 * Class Tree
 * @package Fitcher\Entity
 */
class Tree implements TreeContract
{
    /**
     * @var BranchContract|null
     */
    private $root = null;
    /**
     * @var int
     */
    private $maxDeepGeneration = 2;
    /**
     * @var int
     */
    private $maxLeafsValue = 1;
    /**
     * @var int
     */
    private $maxLeafsSum = 1;

    private $leavesLeafsNumbers = null;

    /**
     * Tree constructor.
     * @param BranchContract|null $root
     * @param int $maxDeepGeneration
     * @param int $maxLeafsValue
     * @param int $maxLeafsSum
     */
    public function __construct(BranchContract $root = null, int $maxDeepGeneration = 2, int $maxLeafsValue = 1, int $maxLeafsSum = 1)
    {
        $this->root = $root;
        $this->maxDeepGeneration = $maxDeepGeneration;
        $this->maxLeafsValue = $maxLeafsValue;
        $this->maxLeafsSum = $maxLeafsSum;
    }

    /**
     * @param BranchContract $root
     */
    public function setRoot(BranchContract $root)
    {
        $this->root = $root;
    }

    /**
     * @return Branch
     */
    public function getRoot(): Branch
    {
        return $this->root;
    }

    /**
     * @param int $maxDeep
     * @param int $maxLeafsValue
     */
    public function generateTree(int $maxDeep, int $maxLeafsValue)
    {

    }

    public function findLeafs()
    {
        //$this->root
    }

    /**
     * @param int $maxLeafsSum
     */
    public function sortLeafs(int $maxLeafsSum)
    {
        //$this->root->recursiveGoint($this->maxLeafsSum);
    }


    /**
     *
     */
    public function printTree()
    {
        var_dump($this->root);
    }

    /**
     *
     */
    public function printLeavesNumbers()
    {
        var_dump($this->leavesLeafsNumbers);
    }
}