<?php

namespace Fitcher\Entity\Contract;

interface TreeContract
{
    public function setRoot(BranchContract $root);

    public function getRoot();

    public function generateTree(int $max_deep, int $max_leafs_value);

    public function sortLeafs(int $max_sum);

    public function printTree();

    public function printLeavesNumbers();

}