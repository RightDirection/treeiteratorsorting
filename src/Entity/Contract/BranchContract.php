<?php

namespace Fitcher\Entity\Contract;

interface BranchContract
{
    public function getLeft();

    public function setLeft(BranchContract $Left);

    public function getRight();

    public function setRight(BranchContract $Right);

    public function hasLeafs();

    public function getLeafs();

    public function setLeafs(LeafContract $leafs);

    
}