<?php

namespace Fitcher\Entity\Contract;

interface LeafContract
{
    public function __construct(int $value = 0, LeafContract $next = null);

    public function getValue();

    public function setValue(int $value);

    public function getNext();

    public function setNext(LeafContract $next = null);

    public function hasNext();

}